# Pior Filme API

## Getting started

Esta API RESTful possibilita a leitura da lista de indicados e vencedores
da categoria Pior Filme do ***[Golden Raspberry Awards](https://en.wikipedia.org/wiki/Golden_Raspberry_Awards)***.

![Golden_Raspberry_Award](https://upload.wikimedia.org/wikipedia/en/3/35/Golden_Raspberry_Award.jpg)

Ela também lista o produtor com maior intervalo entre dois prêmios consecutivos, e o que
obteve dois prêmios mais rápido, de acordo com o exemplo em JSON abaixo.

Esta API foi construída em linguagem JAVA e é executada através de uma JRE ou JDK.

```json
{
    "min": [
        {
            "producer": "Producer 1",
            "interval": 1,
            "previousWin": 2008,
            "followingWin": 2009
        },
        {
            "producer": "Producer 2",
            "interval": 1,
            "previousWin": 2018,
            "followingWin": 2019
        }
    ],
    "max": [
        {
            "producer": "Producer 1",
            "interval": 99,
            "previousWin": 1900,
            "followingWin": 1999
        },
        {
            "producer": "Producer 2",
            "interval": 99,
            "previousWin": 2000,
            "followingWin": 2099
        }
    ]
}
```
***
## Running First Time!

Para rodar a api em sua estação ou servidor, será necessário seguir
os passos abaixo.

### Download

- [ ] Baixar o arquivo [pior-filme-api-0.1.8-SNAPSHOT.jar](https://gitlab.com/juceliohv/pior-filme-api/-/raw/main/pior-filme-api-0.1.8-SNAPSHOT.jar?inline=false)
- [ ] Baixar o [JRE ou JDK 11](https://adoptopenjdk.net/) ou versão superior.
- [ ] Baixar o [Maven](https://maven.apache.org/download.cgi).

### Configuration

- [ ] [Instalar a JRE ou JDK](https://www.java.com/pt-BR/download/help/download_options_pt-br.html). Em caso de arquivo compactado zip ou 
tar.gz, descompactar no diretório de sua escolha.
- [ ] Configurar nas variáveis de ambiente do Sistema Operacional a [variável JAVA_HOME](https://docs.oracle.com/cloud/help/pt_BR/pbcs_common/DIEPM/epm_set_java_home_104x6dd63633_106x6dd6441c.htm#DIEPM-GUID-7D734C69-2DE8-4E93-A3C8-9C3F6AD12D1B) 
apontando para o diretório onde foi instalado a JRE ou JDK.
- [ ] Configura a variável de ambiente PATH, incluindo a variável JAVA_HOME concatenada ao diretório bin. Ex.: %JAVA_HOME%\bin.
- [ ] [Instalar o Maven](https://maven.apache.org/install.html).
- [ ] Configura a variável de ambiente PATH, incluindo a variável M2_HOME, que aponta para a instalação do maven,
concatenada ao diretório bin. Ex.: %M2_HOME%\bin.

### Run Integrated Tests

Para executar os testes é necessário
chamar o java via linha de comando do seu respectivo sistema operacional. CMD ou PowerShell no Windows e o Shell no
Linux, por exemplo.
- [ ] Abrir o prompt/console de linha de comando.
- [ ] Navegar até o diretório do projeto clonado via git ou baixado via zip.
- [ ] Executar o comando abaixo:

```shell
      mvn test
```
- [ ] Verificar o log através do console de linha de comando.


### Run

Para executar o arquivo pior-filme-api-0.1.8-SNAPSHOT.jar é necessário
chamar o java via linha de comando do seu respectivo sistema operacional. CMD ou PowerShell no Windows e o Shell no 
Linux, por exemplo.
- [ ] Abrir o prompt/console de linha de comando.
- [ ] Navegar até o diretório onde está o arquivo pior-filme-api-0.1.8-SNAPSHOT.jar.
- [ ] Executar o comando abaixo:

```shell
      java -jar ./pior-filme-api-0.1.1-SNAPSHOT.jar
      
      ## ou no diretório onde o projeto foi clonado via git ou baixado via zip
      
      mvn spring-boot:run
```
- [ ] Verificar o log através do console de linha de comando.
### Host

A API será servida em seu desktop ou servidor através da porta [8080](http://localhost:8080/).

````html
http://localhost:8080
````
### Database

O banco de dados utilizado é o H2. O banco é instanciado apenas
em tempo de execução da API em memória RAM. Após o encerramento da
execução da API, a instância do H2 é encerrada e seus dados são
perdidos.

Existem duas formas de acessar os dados do banco H2 de forma 
visual.

- [ ] Console H2 embarcado

- [ ] Cliente de banco de dados compativel com conexão ao H2.

#### Acesso via Console H2

> 1. É necessário acessar o link [http://localhost:8080/h2-console](http://localhost:8080/h2-console)
> 2. Em JDBC URL deve ser informado ___jdbc:h2:mem:test_db___.
> 3. Em user name deve ser informado ___SA___.
> 4. Em password não deve ser informado nada. Deve permanecer em branco.
> 
>   ![h2_logon](/assets/h2_logon.png)
>    
> * Exemplo de sessão  
>    
>   ![h2_session](/assets/h2_session.png)

#### Acesso via client database ([DBeaver](https://dbeaver.io/))

Neste caso é necessário utilizar um programa client de banco de 
dados que realizará o acesso ao SGBD. Para fins didáticos utilizaremos
o DBeaver.

> 1. É necessário acessar o link [Download](https://dbeaver.io/download/)
> 2. Fazer o Download do Instalador do client, no caso o DBeaver.
> 3. Instalar a distribuição de acondo com o seu sistema operacional(windows, linux, mac, etc).
> 4. Executar o client e configurar a conexão ao banco H2.
> 5. Criar uma conexão do tipo H2 Server.
>   ![dbeaver_connection](/assets/dbeaver_connection.png)
> 6. Configurar a conexão, onde: 
>   ![dbeaver_test](/assets/dbeaver_test.png)
>    1. Host = localhost
>    2. Port = 9092
>    3. Database/Schema = mem:test_db
>    4. Username = sa
>    5. Password deve ficar em branco
>    6. Testar a Conexão.
> 7. Abri-lá e efetuar as consultas desejadas.
> 
>   ![dbeaver_session](/assets/dbeaver_session.png)

*** 
## Using API

Agora vamos, enfim, colocar a mão na massa!

A seguir veremos como consumir a API através do Postman. Como consultar
a documentação dos endpoints da API via Swagger. Como limpar a base
de dados via API. Como fazer o upload de sua carga de dados personalizada.
E finalmente como extrar o resultado dos vencedores com menor e maior intervalor
entre os prêmios.

### Postman

Após inicar a API, seus endpoints podem ser consumidos através do **[Postman](https://www.postman.com/)**.

Para isto, siga os passos abaixo.
> 1. Baixar e instalar o **[Postman](https://www.postman.com/downloads/)**.
> 2. Executar o Postman.
> 3. Importar a Collection disponível neste repositório. 
> Clique **[aqui](./pior filme api.postman_collection.json)** para baixar.
> 4. Utilizar os endpoints.
> 
>   ![postman_session](/assets/postman_session.png)

### Swagger 

Swagger é uma biblioteca que permite a documentação de uma api
diretamente em seu código fonte. Também possibilita o gerenciamento e 
o teste das mesmas através de uma interface gráfica web.

- [ ] Para acessar o swagger desta API, basta acessar o link 
***[http://localhost:8080/swagger-ui/index.html#/](http://localhost:8080/swagger-ui/index.html#/)***
durante o tempo de execução da API.

    ![swagger_session](/assets/swagger_session.png)

    ![swagger_try](/assets/swagger_try.png)

- [ ] Também é possível gerar a documentação da API via Json utilzando
o OpenAPI 3.0, acessando o link 
***[http://localhost:8080/v3/api-docs](http://localhost:8080/v3/api-docs)***.

### Clear Database

- [ ] Para limpar a base de dados em tempo de execução da API, basta 
chamar o endpoint 
[/api/prize/all](http://localhost:8080/swagger-ui/index.html#/prize-controller/deleteAll), 
tanto pelo postman, quanto pelo swagger.

### Upload your data file

- [ ] Para realizar o upload de seu arquivo CSV personalizado, 
siga os passos:
> 1. Criar um arquivo CSV com o seguinte schema de preenchimento.
>````
>year;title;studios;producers;winner
>1980;Can't Stop the Music;Associated Film Distribution;Allan Carr;yes
>1980;Cruising;Lorimar Productions, United Artists;Jerry Weintraub;
>1980;The Formula;MGM, United Artists;Steve Shagan;
>````
| year | title | studios | producers | winner |
|---|---|---|---|---|
| 1980 | Can't Stop the Music | Associated Film Distribution         | Allan Carr | yes    |
| 1980 | Cruising | Lorimar Productions, United Artists  | Jerry Weintraub |        |
| 1980 | The Formula | MGM, United Artists                  | Steve Shagan |        |
>
>
> 2. Limpar a base de dados de acordo com o item ***[Clear Database](#clear-database)***.
>
> 
> 3. Em tempo de execução da API, chamar o endpoint
     ***/api/upload/csv***.
>    1. Swagger
>       1. Abrir a seção do upload na página do swagger ***[/api/upload/csv](http://localhost:8080/swagger-ui/index.html#/upload-controller/uploadCSVFile)***.
>       2. Selecionar o botão ***Try it out***.
>       3. No campo ***file*** selecionar o botão ***Choose File***.
>       4. Escolher o arquivo CSV a ser carregado.
>       5. Selecionar o botão ***Execute***.
>       6. Verificar se o código de retorno HTTP foi igual a ***200***.
>       ![swagger-upload](/assets/swagger_upload.png)
>    2. Postman
>       1. Abrir a requisição ***/api/upload/csv***.
>       2. Na aba ***body***, selecionar a opção ***form-data***.
>       3. No campo key, escolher file e depois escrever ***file***.
>       4. No campo Value selecionar o botão ***select file*** e escolher
o arquivo CSV a ser carregado.
>       5. Enviar a requisição ***POST*** selecionando o botão ***SEND***.
>       6. Verificar se o código do retorno HTTP foi igual a ***200***.
>       ![postman-send](/assets/postman_send.png)
### Getting results

- [ ] Para obter a lista de vencedores que possuem o maior e o menor
intervalo de anos entre cada vitória, basta chamar metodo GET do 
endpoint ***[/api/prize/winners](http://localhost:8080/swagger-ui/index.html#/prize-controller/getProducers)***.
![swagger-winners](/assets/swagger_winners.png)
