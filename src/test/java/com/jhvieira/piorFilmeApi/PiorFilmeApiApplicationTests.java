package com.jhvieira.piorFilmeApi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jhvieira.piorFilmeApi.model.dto.PrizeDto;
import org.junit.Ignore;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment =
		SpringBootTest.WebEnvironment.MOCK, classes = PiorFilmeApiApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
		locations = "classpath:application-integrationtest.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PiorFilmeApiApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	@Order(1)
	public void findOne() throws Exception {
		mockMvc.perform(get("/api/prize/116"))
				.andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(116)))
				.andExpect(jsonPath("$.title", is("Swept Away")))
				.andExpect(jsonPath("$.year", is(2002)))
				.andExpect(jsonPath("$.studios[0].id", is(29)))
				.andExpect(jsonPath("$.studios[0].name", is("Screen Gems")))
				.andExpect(jsonPath("$.producers[0].id", is(152)))
				.andExpect(jsonPath("$.producers[0].name", is("Matthew Vaughn")))
				.andExpect(jsonPath("$.winner", is(true)));
	}

	@Test
	@Order(2)
	public void findWinners() throws Exception {
		mockMvc.perform(get("/api/prize/winners"))
				.andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.min[0].producer", is("Joel Silver")))
				.andExpect(jsonPath("$.min[0].interval", is(1)))
				.andExpect(jsonPath("$.min[0].previousWin", is(1990)))
				.andExpect(jsonPath("$.min[0].followingWin", is(1991)))
				.andExpect(jsonPath("$.max[0].producer", is("Matthew Vaughn")))
				.andExpect(jsonPath("$.max[0].interval", is(13)))
				.andExpect(jsonPath("$.max[0].previousWin", is(2002)))
				.andExpect(jsonPath("$.max[0].followingWin", is(2015)));
	}

	@Test
	@Order(3)
	public void deleteOne() throws Exception {
		mockMvc.perform(delete("/api/prize/117"))
				.andDo(print())
				.andExpect(status().isOk());

		mockMvc.perform(get("/api/prize/117"))
				.andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	@Order(4)
	public void deleteAll() throws Exception {
		mockMvc.perform(delete("/api/prize/all"))
				.andDo(print())
				.andExpect(status().isOk());

		mockMvc.perform(get("/api/prize/1"))
				.andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	@Order(5)
	public void uploadCsvAfterCleanDatabase() throws Exception {

		mockMvc.perform(delete("/api/prize/all"))
				.andDo(print())
				.andExpect(status().isOk());

		InputStream is = getClass().getResourceAsStream("/static/movielist.csv");

		MockMultipartFile csvFile = new MockMultipartFile("file",
				"movielist.csv", "text/plain", is.readAllBytes());

		mockMvc.perform(multipart("/api/upload/csv")
				.file(csvFile))
				.andDo(print())
				.andExpect(status().isOk());

		mockMvc.perform(get("/api/prize/207"))
				.andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(207)))
				.andExpect(jsonPath("$.title", is("Can't Stop the Music")))
				.andExpect(jsonPath("$.year", is(1980)))
				.andExpect(jsonPath("$.winner", is(true)));

		mockMvc.perform(get("/api/prize/412"))
				.andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(412)))
				.andExpect(jsonPath("$.title", is("Rambo: Last Blood")))
				.andExpect(jsonPath("$.year", is(2019)))
				.andExpect(jsonPath("$.winner", is(nullValue())));
	}

	@Test
	@Order(6)
	public void createOne() throws Exception {
		List<String> studioNames = new ArrayList<>();
		studioNames.add("Studio Test");
		List<String> producerNames = new ArrayList<>();
		producerNames.add("Producer Test");
		PrizeDto prize = new PrizeDto(
				2022,
				"Test Title",
				studioNames,
				producerNames,
				true);

		mockMvc.perform(post("/api/prize")
				.content(asJsonString(prize))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(413)))
				.andExpect(jsonPath("$.title", is(prize.getTitle())))
				.andExpect(jsonPath("$.year", is(prize.getYear())))
				.andExpect(jsonPath("$.studios[0].name",
						is(prize.getStudios().get(0).getName())))
				.andExpect(jsonPath("$.producers[0].name",
						is(prize.getProducers().get(0).getName())))
				.andExpect(jsonPath("$.winner", is(true)));
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
