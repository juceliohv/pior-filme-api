package com.jhvieira.piorFilmeApi.service;

import com.jhvieira.piorFilmeApi.Exceptions.DtoParseException;
import com.jhvieira.piorFilmeApi.Exceptions.NullProducerException;
import com.jhvieira.piorFilmeApi.Exceptions.NullStudioException;
import com.jhvieira.piorFilmeApi.Exceptions.TitleAlreadyExistsException;
import com.jhvieira.piorFilmeApi.model.dto.PrizeDto;
import com.jhvieira.piorFilmeApi.model.dto.ResultDto;
import com.jhvieira.piorFilmeApi.model.dto.ResultProducerDto;
import com.jhvieira.piorFilmeApi.model.sql.Producer;
import com.jhvieira.piorFilmeApi.model.sql.Studio;
import com.jhvieira.piorFilmeApi.model.sql.Title;
import com.jhvieira.piorFilmeApi.repository.ProducerRepository;
import com.jhvieira.piorFilmeApi.repository.StudioRepository;
import com.jhvieira.piorFilmeApi.repository.TitleRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class PrizeService {

    Logger logger = LoggerFactory.getLogger(PrizeService.class);

    @Autowired
    private TitleRepository titleRepository;

    @Autowired
    private ProducerRepository producerRepository;

    @Autowired
    private StudioRepository studioRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRES_NEW)
    public Title create(PrizeDto prize) {

        logger.info(">>> Iniciando criação de registro!");
        if (titleRepository.existsByTitle(prize.getTitle()))
            throw new TitleAlreadyExistsException();

        logger.info(">>> Criando registro: " + prize);

        Title title;
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {

            Validator validator = factory.getValidator();
            Set<ConstraintViolation<PrizeDto>> violations = validator.validate(prize);

            if (!violations.isEmpty()) {
                violations.forEach(violation -> logger.error(violation.getMessage()));
                throw new DtoParseException(violations
                        .stream().map(ConstraintViolation::getMessage)
                        .reduce("", String::concat));
            }

            title = convertPrizeDtoToTitleEntity(prize);
            List<Studio> studios = convertPrizeDtoToStudioEntityList(prize);
            List<Producer> producers = convertPrizeDtoToProducerEntityList(prize);

            title.getProducers().clear();
            title.getProducers().addAll(producers);
            title.getStudios().clear();
            title.getStudios().addAll(studios);

            if (title.getStudios().size() == 0)
                throw new NullStudioException("Informação sobre os estúdios ausente! Informe ao menos um estúdio.");
            for (Studio studio : studios) {
                studio.getTitles().add(title);
                if (studioRepository.findByName(studio.getName()).isEmpty())
                    studioRepository.save(studio);
            }

            if (title.getProducers().size() == 0)
                throw new NullProducerException("Informação sobre os produtores ausente! Informe ao menos um produtor.");
            for (Producer producer : producers) {
                producer.getTitles().add(title);
                if (producerRepository.findByName(producer.getName()).isEmpty())
                    producerRepository.save(producer);
            }
        }
        logger.info(">>> Finalizando criação de registro!");
        return titleRepository.save(title);
    }

    public Optional<Title> findById(Integer id) {

        return titleRepository.findById(id);
    }

    public void delete(Title titulo) {

        logger.info(">>> Eliminando registro: " + titulo.toString());

        for (Studio studio : titulo.getStudios()) {
            studio.getTitles().remove(titulo);
            studioRepository.save(studio);
        }
        for (Producer producer : titulo.getProducers()) {
            producer.getTitles().remove(titulo);
            producerRepository.save(producer);
        }
        titleRepository.deleteById(titulo.getId());
        logger.info(">>> Registro eliminado!");
    }

    public void deleteAll() {

        logger.info(">>> Limpando banco de dados!");
        titleRepository.deleteAll();
        studioRepository.deleteAll();
        producerRepository.deleteAll();
        logger.info(">>> Banco de dados vazio!");
    }

    private Title convertPrizeDtoToTitleEntity(PrizeDto prize) throws ParseException {
        return modelMapper.map(prize, Title.class);
    }

    private List<Studio> convertPrizeDtoToStudioEntityList(PrizeDto prize) throws ParseException {
        List<Studio> convertedList = new ArrayList<>();
        for (Studio std : prize.getStudios()) {
            Optional<Studio> persistedStudio = studioRepository.findByName(std.getName());
            if (persistedStudio.isPresent())
                convertedList.add(persistedStudio.get());
            else
                convertedList.add(std);
        }
        return convertedList;
    }

    private List<Producer> convertPrizeDtoToProducerEntityList(PrizeDto prize) throws ParseException {
        List<Producer> convertedList = new ArrayList<>();
        for (Producer prd : prize.getProducers()) {
            Optional<Producer> persistedProducer = producerRepository.findByName(prd.getName());
            if (persistedProducer.isPresent())
                convertedList.add(persistedProducer.get());
            else
                convertedList.add(prd);
        }
        return convertedList;
    }

    public ResultDto getWinnerProducers() {

        Collection<Title> titulos = titleRepository.findByWinner(true);

        TreeMap<String, TreeSet<Integer>> titulosPorProdutor = new TreeMap<>();
        
        titulos.forEach(titulo ->
                addProducerEntry(titulosPorProdutor, titulo));

        Map<String, TreeSet<Integer>> filteredWinMap = titulosPorProdutor.entrySet()
                .stream().filter(titulo -> titulo.getValue().size() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        return mountResultDto(filteredWinMap);
    }

    private ResultDto mountResultDto(Map<String, TreeSet<Integer>> filteredWinMap) {

        if (filteredWinMap.size() == 0)
            return null;

        Set<ResultProducerDto> producerDtoSet = new TreeSet<>();
        filteredWinMap.forEach((key, value) -> {
            AtomicReference<Integer> prev = new AtomicReference<>();
            value.forEach( val -> {
                if (Objects.nonNull(prev.get())) {
                    ResultProducerDto dto = new ResultProducerDto(
                            key, val - prev.get(), prev.get(), val);
                    producerDtoSet.add(dto);
                }
                prev.set(val.intValue());
            });
        });

        ResultDto resultDto = new ResultDto();

        ResultProducerDto firstDto = producerDtoSet.stream()
                .reduce((first, last) -> first)
                .orElse(null);
        Set<ResultProducerDto> min = producerDtoSet.stream()
                .filter(prod -> prod.getInterval().equals(firstDto.getInterval()))
                .collect(Collectors.toSet());

        ResultProducerDto lastDto = producerDtoSet.stream()
                .reduce((first, last) -> last)
                .orElse(null);
        Set<ResultProducerDto> max = producerDtoSet.stream()
                .filter(prod -> prod.getInterval().equals(lastDto.getInterval()))
                .collect(Collectors.toSet());

        resultDto.getMin().add(min.stream().reduce((first, second) -> first).orElse(null));
        resultDto.getMin().add(min.stream().reduce((first, second) -> second).orElse(null));

        resultDto.getMax().add(max.stream().reduce((first, second) -> first).orElse(null));
        resultDto.getMax().add(max.stream().reduce((first, second) -> second).orElse(null));

        return resultDto;
    }

    private void addProducerEntry(TreeMap<String, TreeSet<Integer>> titulosPorProdutor, Title titulo) {

        for (Producer prod : titulo.getProducers()) {

            TreeSet<Integer> entry = titulosPorProdutor.get(prod.getName());
            if (Objects.isNull(entry)) {
                TreeSet<Integer> years = new TreeSet<>();
                years.add(titulo.getYear());
                titulosPorProdutor.put(prod.getName(), years);
            } else {
                entry.add(titulo.getYear());
            }
        }
    }
}
