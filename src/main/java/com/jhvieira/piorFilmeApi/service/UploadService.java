package com.jhvieira.piorFilmeApi.service;

import com.jhvieira.piorFilmeApi.model.csv.PrizeCsv;
import com.jhvieira.piorFilmeApi.model.dto.PrizeDto;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.MappingStrategy;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class UploadService {

    Logger logger = LoggerFactory.getLogger(UploadService.class);
    @Autowired
    private PrizeService prizeService;
    @Autowired
    private ModelMapper modelMapper;

    public String importCsvEntries(InputStream is, Model model) {

        logger.info(">>> Iniciando leitura das linhas do arquivo CSV!");
        MappingStrategy<PrizeCsv> mappingStrategy = new HeaderColumnNameMappingStrategy<>();
        mappingStrategy.setType(PrizeCsv.class);
        try (Reader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {

            CsvToBean<PrizeCsv> csvToBean = getCsvBuilder(reader, mappingStrategy);

            List<PrizeCsv> entries = csvToBean.parse();
            List<PrizeDto> listaDto = new ArrayList<>();
            AtomicBoolean hasViolations = new AtomicBoolean(false);

            try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {

                Validator validator = factory.getValidator();
                entries.forEach(prize -> listaDto.add(convertCsvToDto(prize)));
                AtomicInteger line = new AtomicInteger();
                listaDto.forEach(dto -> {
                    dto.setLineNumber(line.incrementAndGet());
                    Set<ConstraintViolation<PrizeDto>> violations = validator.validate(dto);
                    violations.forEach(violation -> {
                        logger.error(violation.getMessage());
                        model.addAttribute("line " + dto.getLineNumber() + ":", violation.getMessage());
                        hasViolations.set(true);
                    });
                });
            }

            if (!hasViolations.get()) {
                listaDto.forEach(dto -> prizeService.create(dto));
                model.addAttribute("entries", entries);
                model.addAttribute("status", true);
            } else {
                model.addAttribute("status", false);
            }

        } catch (Exception ex) {
            model.addAttribute("message", "Um erro ocorreu durante ao processamento do " +
                    "arquivo CSV." + ex.getMessage());
            model.addAttribute("status", false);
            logger.error(ex.getMessage(), ex);
        }
        logger.info(">>> Finalizando leitura das linhas do arquivo CSV!");

        return model.toString();
    }

    private CsvToBean<PrizeCsv> getCsvBuilder(Reader reader, MappingStrategy<PrizeCsv> mappingStrategy) {
        return new CsvToBeanBuilder(reader)
                .withType(PrizeCsv.class)
                .withMappingStrategy(mappingStrategy)
                .withIgnoreLeadingWhiteSpace(true)
                .withSeparator(';')
                .build();
    }

    private PrizeDto convertCsvToDto(PrizeCsv prize) {
        return modelMapper.map(prize, PrizeDto.class);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void firstDatabaseCharge() throws IOException {

        Model model = new ConcurrentModel();

        InputStream targetStream = getClass().getResourceAsStream("/static/movielist.csv");

        importCsvEntries(targetStream , model);
    }
}
