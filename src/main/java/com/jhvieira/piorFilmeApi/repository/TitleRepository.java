package com.jhvieira.piorFilmeApi.repository;

import com.jhvieira.piorFilmeApi.model.sql.Title;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface TitleRepository extends JpaRepository<Title, Integer> {
    boolean existsByTitle(String title);

    Collection<Title> findByWinner(boolean winner);

}