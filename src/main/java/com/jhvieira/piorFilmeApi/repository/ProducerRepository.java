package com.jhvieira.piorFilmeApi.repository;

import com.jhvieira.piorFilmeApi.model.sql.Producer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProducerRepository extends JpaRepository<Producer, Integer> {
    Optional<Producer> findByName(String name);
}