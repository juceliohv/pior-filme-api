package com.jhvieira.piorFilmeApi.repository;

import com.jhvieira.piorFilmeApi.model.sql.Studio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudioRepository extends JpaRepository<Studio, Integer> {

    Optional<Studio> findByName(String name);
}