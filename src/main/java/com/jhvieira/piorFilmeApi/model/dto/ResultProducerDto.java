package com.jhvieira.piorFilmeApi.model.dto;

import javax.validation.constraints.*;
import java.util.Objects;

public class ResultProducerDto implements Comparable<ResultProducerDto> {

    @NotEmpty(message = "O produtor deve ser informado!")
    @NotNull(message = "O produtor deve ser informado!")
    private String producer;
    @NotNull(message = "Um intervalo deve ser informado!")
    @PositiveOrZero(message = "Informe um número inteiro não negativo!")
    private Integer interval;
    @NotNull(message = "O ano deve ser informado!")
    @Min(value = 1900, message = "Informe um ano superior a 1900!")
    @Max(value = 2100, message = "Informe um ano inferior a 2100!")
    private Integer previousWin;
    @NotNull(message = "O ano deve ser informado!")
    @Size(min = 1900, max = 2100, message = "Informe um valor existente para ano!")
    private Integer followingWin;

    public ResultProducerDto(String producer, Integer interval, Integer previousWin, Integer followingWin) {
        this.producer = producer;
        this.interval = interval;
        this.previousWin = previousWin;
        this.followingWin = followingWin;
    }

    public ResultProducerDto() {
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getPreviousWin() {
        return previousWin;
    }

    public void setPreviousWin(Integer previousWin) {
        this.previousWin = previousWin;
    }

    public Integer getFollowingWin() {
        return followingWin;
    }

    public void setFollowingWin(Integer followingWin) {
        this.followingWin = followingWin;
    }

    @Override
    public String toString() {
        return "ResultProducerDto{" +
                "producer='" + producer + '\'' +
                ", interval=" + interval +
                ", previousWin=" + previousWin +
                ", followingWin=" + followingWin +
                '}';
    }

    @Override
    public int compareTo(ResultProducerDto o) {

        if (    Objects.isNull( o) ||
                Objects.isNull(o.getInterval()))
            return 1;
        if ( this.getInterval() == o.getInterval()) {
            return this.getPreviousWin() - o.getPreviousWin();
        }
        return this.getInterval() - o.getInterval();
    }
}
