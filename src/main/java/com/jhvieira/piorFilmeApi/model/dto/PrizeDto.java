package com.jhvieira.piorFilmeApi.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jhvieira.piorFilmeApi.model.sql.Producer;
import com.jhvieira.piorFilmeApi.model.sql.Studio;
import com.jhvieira.piorFilmeApi.model.sql.Title;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link Title} entity
 */
public class PrizeDto implements Serializable {

    private Integer lineNumber;
    @NotNull(message = "O ano deve ser informado!")
    @Min(value = 1900, message = "Informe um ano superior a 1900!")
    @Max(value = 2100, message = "Informe um ano inferior a 2100")
    private Integer year;
    @NotNull(message = "O título deve ser informado!")
    @NotBlank(message = "O título não pode estar em branco!")
    private String title;
    @NotEmpty(message = "Deve ser informado ao menos um estúdio!")
    private List<@NotBlank(message = "O nome do estúdio deve ser diferente de branco") String> studioNames;
    @NotEmpty(message = "Deve ser informado ao menos um produtor!")
    private List<@NotBlank(message = "O nome do produtor deve ser diferente de branco") String> producerNames;
    private Boolean winner;

    public PrizeDto(Integer year, String title,
                    List<@NotBlank(message = "O nome do estúdio deve ser diferente de branco") String> studioNames,
                    List<@NotBlank(message = "O nome do produtor deve ser diferente de branco") String> producerNames,
                    Boolean winner) {
        this.year = year;
        this.title = title;
        this.studioNames = studioNames;
        this.producerNames = producerNames;
        this.winner = winner;
    }

    public PrizeDto(Integer lineNumber, Integer year, String title,
                    List<@NotBlank(message = "O nome do estúdio deve ser diferente de branco") String> studioNames,
                    List<@NotBlank(message = "O nome do produtor deve ser diferente de branco") String> producerNames,
                    Boolean winner) {
        this.lineNumber = lineNumber;
        this.year = year;
        this.title = title;
        this.studioNames = studioNames;
        this.producerNames = producerNames;
        this.winner = winner;
    }

    public PrizeDto() {
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public List<String> getStudioNames() {
        return studioNames;
    }

    public List<String> getProducerNames() {
        return producerNames;
    }

    public Boolean getWinner() {
        return winner;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStudioNames(List<String> studioNames) {
        this.studioNames = studioNames;
    }

    public void setProducerNames(List<String> producerNames) {
        this.producerNames = producerNames;
    }

    public void setWinner(Boolean winner) {
        this.winner = winner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrizeDto entity = (PrizeDto) o;
        return Objects.equals(this.title, entity.title) &&
                Objects.equals(this.year, entity.year) &&
                Objects.equals(this.studioNames, entity.studioNames) &&
                Objects.equals(this.producerNames, entity.producerNames) &&
                Objects.equals(this.winner, entity.winner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, year, studioNames, producerNames, winner);
    }

    @Override
    public String toString() {
        return "PrizeDto{" +
                "lineNumber=" + lineNumber +
                ", year=" + year +
                ", title='" + title + '\'' +
                ", studioNames=" + studioNames +
                ", producerNames=" + producerNames +
                ", winner=" + winner +
                '}';
    }

    @JsonIgnore
    public List<Studio> getStudios() {
        List<Studio> studios = new ArrayList<>();
        if (Objects.nonNull(getStudioNames())) {
            for (String studio : getStudioNames()) {
                studios.add(new Studio(studio));
            }
        }
        return studios;
    }

    @JsonIgnore
    public List<Producer> getProducers() {
        List<Producer> producers = new ArrayList<>();
        if (Objects.nonNull(getProducerNames())) {
            for (String producer : getProducerNames()) {
                producers.add(new Producer(producer));
            }
        }
        return producers;
    }
}