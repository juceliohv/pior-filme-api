package com.jhvieira.piorFilmeApi.model.dto;

import com.jhvieira.piorFilmeApi.model.sql.Producer;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

public class ProducerDto implements Serializable {
    private final Integer id;
    @NotNull(message = "O nome do produtor deve ser informado!")
    @NotBlank(message = "O nome do produtor não pode estar em branco")
    private final String name;

    public ProducerDto(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProducerDto entity = (ProducerDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.name, entity.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "name = " + name + ")";
    }
}