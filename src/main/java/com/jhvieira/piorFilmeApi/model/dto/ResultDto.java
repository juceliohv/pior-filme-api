package com.jhvieira.piorFilmeApi.model.dto;

import java.util.Set;
import java.util.TreeSet;

public class ResultDto {

    private Set<ResultProducerDto> min = new TreeSet<>();
    private Set<ResultProducerDto> max = new TreeSet<>();

    public ResultDto(Set<ResultProducerDto> min, Set<ResultProducerDto> max) {
        this.min = min;
        this.max = max;
    }

    public ResultDto() {
    }

    public Set<ResultProducerDto> getMin() {
        return min;
    }

    public void setMin(Set<ResultProducerDto> min) {
        this.min = min;
    }

    public Set<ResultProducerDto> getMax() {
        return max;
    }

    public void setMax(Set<ResultProducerDto> max) {
        this.max = max;
    }
}
