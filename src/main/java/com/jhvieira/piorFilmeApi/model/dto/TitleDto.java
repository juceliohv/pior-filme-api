package com.jhvieira.piorFilmeApi.model.dto;

import com.jhvieira.piorFilmeApi.model.sql.Title;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class TitleDto implements Serializable {
    private Integer id;
    @NotNull(message = "O título deve ser informado!")
    @NotBlank(message = "O título não pode estar em branco!")
    private String title;
    @NotNull(message = "O ano deve ser informado!")
    @Min(value = 1900, message = "Informe um ano superior a 1900!")
    @Max(value = 2100, message = "Informe um ano inferior a 2100")
    private Integer year;
    private List<StudioDto> studios;
    private List<ProducerDto> producers;
    private Boolean winner;

    public TitleDto(Integer id, String title, Integer year, List<StudioDto> studios, List<ProducerDto> producers, Boolean winner) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.studios = studios;
        this.producers = producers;
        this.winner = winner;
    }

    public TitleDto(String title, Integer year, List<StudioDto> studios, List<ProducerDto> producers, Boolean winner) {
        this.title = title;
        this.year = year;
        this.studios = studios;
        this.producers = producers;
        this.winner = winner;
    }

    public TitleDto() {
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public List<StudioDto> getStudios() {
        return studios;
    }

    public List<ProducerDto> getProducers() {
        return producers;
    }

    public Boolean getWinner() {
        return winner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TitleDto entity = (TitleDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.title, entity.title) &&
                Objects.equals(this.year, entity.year) &&
                Objects.equals(this.studios, entity.studios) &&
                Objects.equals(this.producers, entity.producers) &&
                Objects.equals(this.winner, entity.winner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, year, studios, producers, winner);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "title = " + title + ", " +
                "year = " + year + ", " +
                "studios = " + studios + ", " +
                "producers = " + producers + ", " +
                "winner = " + winner + ")";
    }
}