package com.jhvieira.piorFilmeApi.model.sql;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "\"tb_producer\"")
public class Producer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"i_id\"")
    private Integer id;
    @Column(unique = true, name = "\"c_name\"", nullable = false)
    private String name;
    @JsonIgnore
    @ManyToMany(mappedBy = "producers")
    private Set<Title> titles = new HashSet<>();

    public Producer(@NonNull String name) {
        this.name = name;
    }

    public Producer(Integer id, @NonNull String name) {
        this.id = id;
        this.name = name;
    }

    public Producer() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public Set<Title> getTitles() {
        return titles;
    }

    public void setTitles(Set<Title> titles) {
        this.titles = titles;
    }
}
