package com.jhvieira.piorFilmeApi.model.sql;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "\"tb_title\"")
public class Title {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"i_id\"")
    private Integer id;
    @Column(unique = true, name = "\"c_title\"", nullable = false)
    private String title;
    @Column(name = "\"i_year\"", nullable = false)
    private Integer year;
    @ManyToMany
    @JoinTable(name="\"tb_title_studio\"",joinColumns=
    {@JoinColumn(name="\"i_title_id\"")},inverseJoinColumns=
        {@JoinColumn(name="\"i_studio_id\"")})
    private Set<Studio> studios = new HashSet<>();
    @ManyToMany
    @JoinTable(name="\"tb_title_producer\"",joinColumns=
    {@JoinColumn(name="\"i_title_id\"")},inverseJoinColumns=
        {@JoinColumn(name="\"i_producer_id\"")})
    private Set<Producer> producers = new HashSet<>();
    @Column(name = "\"b_winner\"")
    private Boolean winner;

    public Title(@NonNull String title, @NonNull Integer year, @NonNull Set<Studio> studios, @NonNull Set<Producer> producers, Boolean winner) {
        this.title = title;
        this.year = year;
        this.studios = studios;
        this.producers = producers;
        this.winner = winner;
    }

    public Title() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Set<Producer> getProducers() {
        return producers;
    }

    public void setProducers(Set<Producer> producers) {
        this.producers = producers;
    }

    public Boolean getWinner() {
        return winner;
    }

    public void setWinner(Boolean winner) {
        this.winner = winner;
    }

    public Set<Studio> getStudios() {
        return studios;
    }

    public void setStudios(Set<Studio> studios) {
        this.studios = studios;
    }
}
