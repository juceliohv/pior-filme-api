package com.jhvieira.piorFilmeApi.model.sql;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "\"tb_studio\"")
public class Studio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"i_id\"")
    private Integer id;
    @Column(unique = true, name = "\"c_name\"", nullable = false)
    private String name;
    @JsonIgnore
    @ManyToMany(mappedBy = "studios")
    private Set<Title> titles = new HashSet<>();
    public Studio(String name) {
        this.name = name;
    }

    public Studio(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Studio() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Title> getTitles() {
        return titles;
    }

    public void setTitles(Set<Title> titles) {
        this.titles = titles;
    }
}
