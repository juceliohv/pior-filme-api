package com.jhvieira.piorFilmeApi.model.csv;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PrizeCsv implements Serializable {

    public static final String VALUE_SEPARATOR_REGEX = "(, and )|( and )|(,)";
    @CsvBindByName
    private Integer year;
    @CsvBindByName
    private String title;
    @CsvBindByName
    private String studios;
    @CsvBindByName
    private String producers;
    @CsvBindByName
    private Boolean winner;

    public PrizeCsv(Integer year, String title, String studios, String producers, Boolean winner) {
        this.year = year;
        this.title = title;
        this.studios = studios;
        this.producers = producers;
        this.winner = winner;
    }

    public PrizeCsv() {
    }

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public String getStudios() {
        return studios;
    }

    public String getProducers() {
        return producers;
    }

    public Boolean getWinner() {
        return winner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrizeCsv entity = (PrizeCsv) o;
        return Objects.equals(this.title, entity.title) &&
                Objects.equals(this.year, entity.year) &&
                Objects.equals(this.studios, entity.studios) &&
                Objects.equals(this.producers, entity.producers) &&
                Objects.equals(this.winner, entity.winner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, year, studios, producers, winner);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "title = " + title + ", " +
                "year = " + year + ", " +
                "studioNames = " + studios + ", " +
                "producerNames = " + producers + ", " +
                "winner = " + winner + ")";
    }

    @JsonProperty("studioNames")
    public List<String> getStudioNames() {
        List<String> studios = new ArrayList<>();
        if (Objects.nonNull(getStudios())) {
            for (String studio : this.getStudios().split(VALUE_SEPARATOR_REGEX)) {
                studios.add(studio.trim());
            }
        }
        return studios;
    }

    @JsonProperty("producerNames")
    public List<String>
    getProducerNames() {
        List<String> producers = new ArrayList<>();
        if (Objects.nonNull(getProducers())) {
            for (String producer : getProducers().split(VALUE_SEPARATOR_REGEX)) {
                producers.add(producer.trim());
            }
        }
        return producers;
    }
}