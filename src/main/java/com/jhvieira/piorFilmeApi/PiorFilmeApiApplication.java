package com.jhvieira.piorFilmeApi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.h2.tools.Server;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.SQLException;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Pior Filme API", version = "1.0", description = "API RESTful para possibilitar a leitura da lista de indicados e vencedores\n" +
		"da categoria Pior Filme do Golden Raspberry Awards."))
public class PiorFilmeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PiorFilmeApiApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean(initMethod = "start", destroyMethod = "stop")
	public Server h2Server() throws SQLException {
		return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9092");
	}
}
