package com.jhvieira.piorFilmeApi.controller;

import com.jhvieira.piorFilmeApi.Exceptions.PrizeNotFoundException;
import com.jhvieira.piorFilmeApi.model.dto.PrizeDto;
import com.jhvieira.piorFilmeApi.model.dto.ResultDto;
import com.jhvieira.piorFilmeApi.model.sql.Title;
import com.jhvieira.piorFilmeApi.service.PrizeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/prize")
public class PrizeController {

    @Autowired
    private PrizeService prizeService;

    @Operation(summary = "Criação de registro de premiação")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Registro criado",
                content = { @Content(mediaType = "application/json",
                schema = @Schema(implementation = Title.class))}),
            @ApiResponse(responseCode = "400", description = "Erro no preenchinto da requisição",
                content = @Content)
    })
    @PostMapping
    public Title create(@Parameter(description = "Json do registro a ser criado")
                                     @RequestBody PrizeDto prize) {

        return prizeService.create(prize);
    }

    @Operation(summary = "Encontra registro de premiação")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Registro encontrado",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Title.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity get(@Parameter(description = "Id do registro na tabela título")
                                  @PathVariable("id") Integer id) {
       return ResponseEntity.ok(
               prizeService.findById(id)
                       .orElseThrow(() -> new PrizeNotFoundException()));

    }

    @Operation(summary = "Elimina o registro de premiação, estudios, produtores e relacionamentos.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Registro eliminado"),
            @ApiResponse(responseCode = "404", description = "Registro não encontrado")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@Parameter(description = "Id do registro na tabela título")
                                     @PathVariable("id") Integer id) {

        Title titulo = prizeService.findById(id)
                .orElseThrow(() -> new PrizeNotFoundException());
        prizeService.delete(titulo);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Elimina todos os registros de premiação, estudios, produtores e relacionamentos.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Registros eliminados")
    })
    @DeleteMapping("/all")
    public ResponseEntity deleteAll() {

        prizeService.deleteAll();
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Lista os ganhadores com maior e menor intervelo entre os premios.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Registros encontrados",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ResultDto.class))})
    })
    @GetMapping("/winners")
    public ResultDto getProducers() {
        return prizeService.getWinnerProducers();
    }
}