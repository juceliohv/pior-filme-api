package com.jhvieira.piorFilmeApi.controller;

import com.jhvieira.piorFilmeApi.model.sql.Title;
import com.jhvieira.piorFilmeApi.service.UploadService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping("/api/upload")
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @Operation(summary = "Upload de arquivo CSV.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Arquivo recebido com sucesso.",
                    content = { @Content(mediaType = MediaType.MULTIPART_FORM_DATA_VALUE,
                            schema = @Schema(implementation = MultipartFile.class))}),
            @ApiResponse(responseCode = "400", description = "Erro no preenchinto da requisição ou do arquivo",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Erro ao processar arquivo no servidor.",
                    content = @Content)
    })
    @PostMapping(value = "/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String uploadCSVFile(@RequestPart("file") MultipartFile file, Model model) {

        try {
            InputStream is = file.getInputStream();
            return uploadService.importCsvEntries(is, model);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
