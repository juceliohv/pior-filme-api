package com.jhvieira.piorFilmeApi.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NullProducerException extends ResponseStatusException {

    public NullProducerException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}
