package com.jhvieira.piorFilmeApi.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class DtoParseException extends ResponseStatusException {
    public static final String ERROR_MESSAGE = "Erro ao converter JSON para Dto!";

    public DtoParseException() {
        super(HttpStatus.BAD_REQUEST, ERROR_MESSAGE);
    }

    public DtoParseException(String violations) {
        super(HttpStatus.BAD_REQUEST, violations);
    }
}
