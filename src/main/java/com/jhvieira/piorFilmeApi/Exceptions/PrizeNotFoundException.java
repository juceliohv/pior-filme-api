package com.jhvieira.piorFilmeApi.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PrizeNotFoundException extends ResponseStatusException {

    public static final String ERROR_MESSAGE = "O registro de premiação não foi encontrado!";

    public PrizeNotFoundException() {
        super(HttpStatus.NOT_FOUND, ERROR_MESSAGE);
    }
}
