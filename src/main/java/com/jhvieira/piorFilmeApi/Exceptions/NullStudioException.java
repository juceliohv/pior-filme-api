package com.jhvieira.piorFilmeApi.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NullStudioException extends ResponseStatusException {

    public NullStudioException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}
