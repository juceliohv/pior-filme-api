package com.jhvieira.piorFilmeApi.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class TitleAlreadyExistsException extends ResponseStatusException {

    private static final String ERROR_MESSAGE = "O título já existe na base de dados!";
    public TitleAlreadyExistsException() {

        super(HttpStatus.BAD_REQUEST, ERROR_MESSAGE);
    }
}
